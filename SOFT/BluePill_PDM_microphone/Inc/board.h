/* 
 * File:   board.h
 */

#ifndef BOARD_H
#define	BOARD_H 201701022


#define BOARD_STM32EV103                   (1)//������� �����
#define STM32                           1//for debug.h


/**
 *  ������ ��� "include"
 */
#include "main.h"
#include "stm32f1xx_hal.h"
//#include "usb_device.h"
//#include "usbd_cdc_if.h"
//#include "conv.h"

/**
 *  ������ ��� "define"
 */
#ifndef BOOL
typedef int8_t				            BOOL;
#endif
//#define bool_t          uint8_t 
#ifndef MSG
    #define MSG                         uint32_t
#endif


//#define ERROR_ACTION(a)              //fERROR_ACTION(a,__MODULE__,__LINE__)

#define SYSTIME                         uint32_t
#define TIC_PERIOD                      (1000UL) //us
#define CPU_FRQ                         (72000000UL) //us

//#define NULL                            (0)
#define TRUE                            1//true
#define FALSE                           0//false

#ifndef TRUE
#define TRUE                            1
#endif
#ifndef FALSE
#define FALSE                           0
#endif

#define FUNCTION_RETURN_OK              1 //- �������� ���������
#define FUNCTION_RETURN_ERROR           0
#define U8                              uint8_t
#define S8                              int8_t
#define VU8                             uint8_t
#define U16                             uint16_t
#define S16                             int16_t
#define U32                             uint32_t
#define S32                             int32_t
#define U64                             uint64_t
#define S64                             int64_t
#define BOOL                            uint8_t


#define u32                             uint32_t
#define s32                             int32_t
#define u16                             uint16_t
#define s16                             int16_t
#define u8                              uint8_t
#define s8                              int8_t
#define vu8                             u8

#define _DEBUG                          1 //commit, if not need debug


// MIC MAIN config ---------------------
#define MIC_MODE_ADC                    0
#define MIC_MODE_PDM                    1


// GPIO --------------------------------
#define KEY_IN                          (GPIOB->IDR & GPIO_IDR_IDR6)

#define TEST_PIN_L                      GPIOB->BSRR = GPIO_BSRR_BR11
#define TEST_PIN_H                      GPIOB->BSRR = GPIO_BSRR_BS11
  
#define TEST_LED_OFF                    GPIOC->BSRR = GPIO_BSRR_BS13
#define TEST_LED_ON                     GPIOC->BSRR = GPIO_BSRR_BR13  
#define TEST_LED_INV                    GPIOC->ODR ^= GPIO_ODR_ODR13
            

// USB ---------------------------------
#define PIN_USB_PULLUP_L                GPIOA->BSRR = GPIO_BSRR_BR10
#define PIN_USB_PULLUP_H                GPIOA->BSRR = GPIO_BSRR_BS10


// SPIFLASH
#define SPIFLASH_GPIO_CS_L              GPIOA->BSRR = GPIO_BSRR_BR15
#define SPIFLASH_GPIO_CS_H              GPIOA->BSRR = GPIO_BSRR_BS15
    
#define SPIFLASH_BLOCK_SIZE             (4096UL)
#define SPIFLASH_BLOCK_NUM              (512UL)
#define SPIFLASH_FLASH_SIZE             (SPIFLASH_BLOCK_NUM * SPIFLASH_BLOCK_SIZE)


#define HAL_SPI1                        1

// for xprintf.h
#define _USE_XFUNC_OUT	                1 // 1: Use output functions
#define	_CR_CRLF		                0 // 1: Convert \n ==> \r\n in the output char
#define _USE_XFUNC_IN	                0 // 1: Use input function
#define	_LINE_ECHO		                0 // 1: Echo back input chars in xgets function

//for conv.h
#define STR_MAX_SIZE                    (65535 -1)

// vor _crc.h
//#define NEED_CRC32_CCITT        1 
#define NEED_CRC16                      1
//#define NEED_CRC8               1
//#define NEED_CRC8_DS            1

//#define __NOP()               
#define __DI()                          __disable_irq() // do { #asm("cli") } while(0) // Global interrupt disable
#define __EI()                          __enable_irq() //do { #asm("sei") } while(0) // Global interrupt enable
#define __CLRWDT()                      do { IWDG->KR = 0x0000AAAA; } while (0) // ������������ �������

#include "_fifo.h"


            
#ifdef	__cplusplus
extern "C" {
#endif
 

#ifdef	__cplusplus
}
#endif

#endif	/* BOARD_H */
