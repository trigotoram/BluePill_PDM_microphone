(EN) BluePill Wav USB recorder with PDM microphone (CIC filter) and SPI FLASH.
(UA) BluePill Wav USB рекордер з мікрофоном PDM (фільтр ) та SPI FLASH.
(RU) BluePill Wav USB рекордер с PDM микрофоном (CIC фильтр) и SPI FLASH.

1 Каталоги:

	1.1 HARDWARE - схема подключения
	1.2 SOFT - исходники для IAR, так же содержит проект для CubeMX. (еще в процессе доработки, компиляция не возможна пока).
	1.3 DOC - доступная документация, pdfs...
	1.4 PIC - картинки, etc

2 Назначение:

	2.1 При первом подключении платы BluePill к USB PC под управлением Windows 7 и выше
	устройство инициализируеться, и появляеться в системе как USB флэш накопитель.
	Если это первое подключение, то потребуеться форматирование, для этого нужно 
	выбрать FAT16 и размер кластер 4 килобайта.
	После успешного форматирования можно нажимать кнопку "Record" ("Запись") и
	производить звуки, которые посредством микрофона должны начать сохраняться на SPI FLASH.
	Прогресс записи визуально можно контролировать наблюдая за периодическим 
	подмаргиванием тестового светодиода на BluePill, который подключен к порту PC13.
	Устройство на момент записи отмонтировывается! Когда запись должна быть окончена,
	нажимаеться эта же кнопка, тесовый светодиод должен погаснуть, сигнализируя тем, что запись успешна.
	Так же должен в системе опять и снова появиться USB флешь накопитель,
	где появиться новый файл "rec.wav". Его можно скопировать и прослушать
	стандартными средствами пользовательской операционной системы.
	
	Есть предкомпилированный файл BluePill_PDM_microphone.hex
	и готовый для тестового прослушивания файл rec.wav


3 Схема и особенности:

	3.1 Схема собрана навесным монтажем, на базе платы BluePill (синяя, оригинальный чип)
	c микроконтроллером STM32F103C8 и PDM микрофона изъятого из платы
	мобильного телефона неизвестной серии и марки и распаян на мелкой макетной платке.
	Ближайший аналог микрофона - MAB-03A-T-D1, его распиновка указана в документе
	eba-06g-l18-2.pdf.
	Микросхема флешь памяти - Winbond W25Q16 - Winbond-w25q32.pdf, распаяна на малой макетной плате.
	Кнопка - обычная тактовая, без внешней подтяжки к VDD, используеться встроенная в МК.
	На плате сделана доработка, отпаян резистор подтяжки R10 и подключен внешний в 2.2кОм
	к порту МК PA10 (смотри stm32f103c8t6_schematic.png)

4 Встроенное программное обеспечение и его особенности:

	4.1 Используеться стандартный стек USB MSD от ST, фильтр CIC и
	полупрограммный дециматор и т.д.
	Данные через модуль SPI загружаються в буфер в ОЗУ посредством DMA,
	по фактам заполнения полубуферов вызывается фильтрация, децимация
	и помещение в FIFO. В главном потоке сканируеться кнопка и от
	ее состояние либо выключает USB и включает запись, либо завершает запись и включает USB.
	Перезаписываеться все время один и тот же файл rec.wav


5 Использованные данные из сети

	5.1 
	https://github.com/olegv142/pdm_fir/blob/master/pdm_fir.c
	http://cdeblog.ru/post/converting-st-link-into-a-j-link/
	http://www.avislab.com/blog/stm32-pwr/
	https://habrahabr.ru/post/316990/
	https://github.com/PUT-PTM/STMwavPlayerMR
	http://we.easyelectronics.ru/STM32/vosproizvedenie-zvuka-na-stm32-discovery-pri-pomoschi-speex.html
	http://radiokot.ru/forum/viewtopic.php?f=59&t=137960
	http://www.st.com/content/ccc/resource/technical/document/application_note/05/fb/41/91/39/02/4d/1e/CD00259245.pdf/files/CD00259245.pdf/jcr:content/translations/en.CD00259245.pdf
	http://people.ece.cornell.edu/land/courses/ece4760/FinalProjects/f2016/bac239_aw528_rk534/bac239_aw528_rk534/bac239_aw528_rk534/index.html
	http://www.avislab.com/blog/stm32-pwr/
	https://habrahabr.ru/post/316990/
	https://github.com/PUT-PTM/STMwavPlayerMR
	http://we.easyelectronics.ru/STM32/vosproizvedenie-zvuka-na-stm32-discovery-pri-pomoschi-speex.html
	http://radiokot.ru/forum/viewtopic.php?f=59&t=137960
	http://www.st.com/content/ccc/resource/technical/document/application_note/05/fb/41/91/39/02/4d/1e/CD00259245.pdf/files/CD00259245.pdf/jcr:content/translations/en.CD00259245.pdf
	http://people.ece.cornell.edu/land/courses/ece4760/FinalProjects/f2016/bac239_aw528_rk534/bac239_aw528_rk534/bac239_aw528_rk534/index.html
	https://geektimes.ru/post/283184/
